import numpy as np
import math
import time

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection

# For run-time analysis purposes, let:
#   N = the number of nodes
#   S = the number of simplices
#   D = the number of dimensions
#
# Both the following are related to D by some power:
#   E = the number of edges per simplex
#   F = the number of faces per simplex


class simplex():
    """Represents a single n-dimensional simplex."""
    def __init__(self, _ind, _l = 0):
        self.ind = _ind
        self.nd = len(_ind) - 1
        self.l = _l

    def set_mesh(self, _mesh):
        """Links the parent mesh to this simplex.

        Runtime: O(1)
        """

        self.mesh = _mesh

    def get_edge_ind(self, ei):
        return [self.ind[ni] for ni in self.mesh.edges[ei]]

    def get_face_ind(self, fi):
        return [self.ind[ni] for ni in self.mesh.faces[fi]]

    def get_node(self, ni):
        return np.array(self.mesh.get_node(ni))

    def edge_length(self, ei):
        edge_ind = self.get_edge_ind(ei)
        p0 = self.get_node(edge_ind[0])
        p1 = self.get_node(edge_ind[1])
        v = p1 - p0
        return np.sqrt(np.sum(np.dot(v,v)))

    def face_area(self, fi):
        face_ind = self.get_face_ind(fi)
        v0 = self.get_node(face_ind[1]) - self.get_node(face_ind[0])
        v1 = self.get_node(face_ind[2]) - self.get_node(face_ind[0])
        v = np.cross(v0, v1)
        return (1.0/2.0) * np.sqrt(np.sum(np.dot(v,v)))

    def volume(self):
        v0 = self.get_node(self.ind[1]) - self.get_node(self.ind[0])
        v1 = self.get_node(self.ind[2]) - self.get_node(self.ind[0])
        v2 = self.get_node(self.ind[3]) - self.get_node(self.ind[0])
        return (1.0/6.0) * np.abs(np.dot(v0, np.cross(v1, v2)))

    def radii_ratio(self):
        vol = self.volume()
        areas = [self.face_area(fi) for fi in range(len(self.mesh.faces))]
        a_sum = sum(areas)
        lengths = [self.edge_length(ei) for ei in range(len(self.mesh.edges))]
        l_max = max(lengths)
        return (6.0 * np.sqrt(6.0)) * vol / (a_sum * l_max)

    def get_angles(self):
        angles = []
        for ni0 in range(len(self.ind)):
            for ni1 in range(len(self.ind)):
                for ni2 in range(len(self.ind)):
                    if (ni0 != ni1) and (ni0 != ni2) and (ni2 != ni1):
                        v0 = self.get_node(self.ind[ni1]) - self.get_node(self.ind[ni0])
                        v1 = self.get_node(self.ind[ni2]) - self.get_node(self.ind[ni0])
                        angles.append(np.arccos(v0.dot(v1) / (np.sqrt(v0.dot(v0)) * np.sqrt(v1.dot(v1)))) * (180.0 / np.pi))

        return angles

    def get_refinement_edge(self):
        """Determines edge to bisect

        This will calculate the edge of this simplex based off edge length. It will return 
        a tuple of the two node indices of the refinement edge.

        Runtime: TODO:
        """
        # k = self.nd - (self.l % self.nd)
        # return k # N.B. This is a hack please remove

        edge_len = [self.edge_length(ei) for ei in range(len(self.mesh.edges))]
        ei = np.argmax(edge_len)
        return ei
 
    def bisect(self):
        """Performs a bisection on this simplex.

        This will calculate the new nodes, and simplices of the bisection operation and add them to the owning mesh.

        Runtime: TODO:
        """

        ei = self.get_refinement_edge()
        redge = tuple(self.mesh.edges[ei])
        new_ind = self.mesh.bisect_edge(tuple(self.get_edge_ind(ei)))
        
        # Create the index lists of the two child elements. The method for this
        # is based off Maubach's paper.

        ind0 = list(self.ind)
        ind0[redge[0]] = new_ind
        child0 = simplex(ind0, self.l + 1)
        self.mesh.add_elem(child0)

        ind1 = list(self.ind)
        ind1[redge[1]] = new_ind
        child1 = simplex(ind1, self.l + 1)
        self.mesh.add_elem(child1)

    def bisect_old(self):
        k = self.nd - (self.l % self.nd)
        new_ind = self.mesh.bisect_edge((self.ind[0],self.ind[k]))

        ind0 = list(self.ind)
        ind0[k] = new_ind
        child0 = simplex(ind0, self.l + 1)

        ind1 = list(self.ind)
        ind1[0:k] = ind1[1:k+1]
        ind1[k] = new_ind
        child1 = simplex(ind1, self.l + 1)

        self.mesh.add_elem(child0)
        self.mesh.add_elem(child1)

class simplex_mesh():
    """Represents a mesh of n-simplices."""

    def __init__(self, _nd):
        assert _nd > 0

        self.nd = _nd
        self.nodes = [[] for i in range(_nd)]
        self.elems = []

        # bisected_edges is used during one iteration of the local_refine 
        # process. It keeps track of all the edges that were bisected and 
        # the index of the new node that was created. This makes it so 
        # determining if a element has a hanging face takes constant time.

        self.bisected_edges = dict()

        # Generate connectivity information once so 
        # that all simplices in the mesh can share it.
        
        self.generate_edge_connectivity()
        self.generate_face_connectivity()

    def generate_edge_connectivity(self):
        self.edges = []
        for i in range(self.nd+1):
            for j in range(i+1, self.nd+1):
                edge = set()
                edge.add(i)
                edge.add(j)
                self.edges.append(edge)

    def generate_face_connectivity(self):
        self.faces = []
        for i in range(self.nd+1):
            for j in range(i+1, self.nd+1):
                for k in range(j+1, self.nd+1):
                    face = set()
                    face.add(i)
                    face.add(j)
                    face.add(k)
                    self.faces.append(face) 

    def add_node(self, pos):
        """Adds a node to a mesh

        Runtime: O(D)
        """

        assert len(pos) == self.nd

        for d in range(self.nd):
            self.nodes[d].append(pos[d])

        return len(self.nodes[0]) - 1

    def get_node(self, ni):
        """Gets the node at index ni as a tuple of coordinates.

        Runtime: O(D)
        """

        assert ni < len(self.nodes[0])

        node  = []
        for d in range(self.nd):
            node.append(self.nodes[d][ni])

        return tuple(node)

    def get_dim(self, di):
        """Returns the list of coordinates for a dimension di.

        Runtime: O(1)
        """

        return self.nodes[di]

    def add_elem(self, elem):
        """Adds a simplex to the mesh.

        Runtime: O(1)
        """

        assert self.nd == elem.nd

        elem.set_mesh(self)
        self.elems.append(elem)

    def bisect_edge(self, edge):
        p0 = self.get_node(edge[0])
        p1 = self.get_node(edge[1]) 

        new_p = tuple([sum(x)/2.0 for x in zip(p0,p1)])
        new_ind = self.add_node(new_p)

        self.bisected_edges[frozenset(edge)] = new_ind

        return new_ind

    def has_hanging_face(self, si):
        """Deterimines if a simplex at index si has a hanging face.

        Runtime: O(E)
        """

        for i in range(len(self.edges)):
            if frozenset(self.elems[si].get_edge_ind(i)) in self.bisected_edges:
                return True

        return False
        
    def local_refine(self, S=None, use_old=False):
        """Performs refinement on selection S of simplices in the mesh.

        Runtime:
        """

        self.bisected_edges.clear()

        if S is None:
            S = range(len(self.elems))

        while len(S) > 0:
            # Bisect all simplices in the selection.

            for i in S:
                if use_old:
                    self.elems[i].bisect_old()
                else:
                    self.elems[i].bisect()

            # Remove the selection from the list of indices to avoid duplicates

            for i in sorted(S, reverse=True):
                del self.elems[i]

            # Generate the list of simplices that need to be refined in the
            # next iteration. This repeats until the whole mesh conforms.

            S = []
            for si in range(len(self.elems)):
                if self.has_hanging_face(si):
                    S.append(si)
            
def plot_mesh(m, ):
    #fig = plt.figure(figsize=(10,10))
    ax = fig.gca(projection='3d')

    x = m.get_dim(0)
    y = m.get_dim(1)
    z = m.get_dim(2)

    lines = set()


    for eli in range(len(m.elems)):
        for edi in range(len(m.edges)):
            lines.add(frozenset(m.elems[eli].get_edge_ind(edi)))

    line_list = [((x[i], y[i], z[i]), (x[j], y[j], z[j])) for (i, j) in lines]

    ax.scatter(x,y,z, s=50)
    ax.add_collection3d(Line3DCollection(line_list, colors='k', linewidths=1))
    #ax.plot_trisurf(x, y, z, triangles=trs, alpha=0.05)

    plt.show()