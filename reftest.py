#%%
import numpy as np
from mesh import *
import matplotlib.pyplot as plt
import time

def setup_mesh(msh):
    msh.add_node((0,0,0))
    msh.add_node((1,0,0))
    msh.add_node((0,1,0))
    msh.add_node((0,0,1))
    msh.add_node((1,1,0))
    msh.add_node((1,0,1))
    msh.add_node((0,1,1))
    msh.add_node((1,1,1))
    msh.add_elem(simplex([0,1,2,3]))
    msh.add_elem(simplex([1,2,4,7]))
    msh.add_elem(simplex([1,2,3,7]))
    msh.add_elem(simplex([2,3,6,7]))
    msh.add_elem(simplex([1,3,5,7]))

def mesh_stat(msh):
    plot_mesh(msh)

    angles = []
    for i in range(len(msh.elems)):
        angles = angles + msh.elems[i].get_angles()

    rr = [msh.elems[i].radii_ratio() for i in range(len(msh.elems))]

    plt.hist(rr, bins=40)
    plt.title('Radii Ratio')
    plt.show()

    plt.hist(angles, bins=40)
    plt.title('All angles')
    plt.show()

def refine_stat(msh, n, use_old_ref):
    num_iters=4

    for i in range(num_iters):
        start = time.time()
        msh.local_refine(use_old=use_old_ref)
        end = time.time()
        print('Mesh %d: Iter %d : Num elems: %d : Elapsed time (ms): %lf' % (n, i, len(msh.elems), (end - start)*1000))


print('-'*50)
print('Shortest Edge Refinement')
m0 = simplex_mesh(3)
setup_mesh(m0)
refine_stat(m0, 0, False)
mesh_stat(m0)

print('-'*20)
print('-'*20)
print('Procedural Refinement')
m1 = simplex_mesh(3)
setup_mesh(m1)
refine_stat(m1, 1, True)
mesh_stat(m1)