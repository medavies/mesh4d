# Serial Refinement for 3D tetrahedral meshes.

This project is a simple python implementation of the serial-refinement algorithm presented in Zhang's paper for 3D tetrahedral meshes. 

The purpose is to build up foundation  of code for generalization to 4D meshes, parallelization, and porting to more performant languages (C/C++)